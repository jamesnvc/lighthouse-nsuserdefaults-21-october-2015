//
//  ViewController.m
//  bloop
//
//  Created by James Cash on 21-10-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *helloLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameField;

@end

@implementation ViewController

- (void)setDisplayLabel:(NSString*)newName
{
    self.helloLabel.text = [NSString stringWithFormat:@"Hello, %@!", newName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *savedName = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
    if (savedName) {
        [self setDisplayLabel:savedName];
    } else {
        NSLog(@"No name saved");
    }
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveName:(id)sender {
    NSString *name = self.nameField.text;
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"userName"];
    [self setDisplayLabel:name];
    self.nameField.text = @"";
}

@end
